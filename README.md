# AI-Camera-Example

Example how to detected Aruco markers with OpenCV.

The size of the marker is 11cm 
with 1 cm white borders. So 13cm x 13cm.

Each aruco code represents a number (id) between 0 - 49.
Each teams will receive 2 aruco markers, one for each bot.

IDs of the teams will be from 10 to 30. 
e.g. Your ids could be 14 & 15. Then the ids of enemies will be 10-13 and 16-30 

## Help

Install numpy and opencv including aruco with pip

> pip install numpy

> pip install opencv-contrib-python

If you problems with installing opencv with aruco, check this stackoverflow topic:
https://stackoverflow.com/questions/45972357/python-opencv-aruco-no-module-named-cv2-aruco
